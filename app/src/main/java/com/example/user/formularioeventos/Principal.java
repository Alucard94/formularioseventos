package com.example.user.formularioeventos;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Principal extends AppCompatActivity {
    private Button salvar, volver, imageButton;
    private CheckBox checkBox;
    private RadioButton btnRed, btnBlue;
    private ToggleButton tglButton;
    private RatingBar ratBar;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        setUpViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setUpViews(){
        imageButton = (Button)findViewById(R.id.imageButton);
        salvar = (Button)findViewById(R.id.salvar);
        volver = (Button)findViewById(R.id.volver);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        btnRed = (RadioButton) findViewById(R.id.radio1);
        btnBlue = (RadioButton) findViewById(R.id.radio2);
        tglButton = (ToggleButton) findViewById(R.id.toggleButton);
        ratBar = (RatingBar) findViewById(R.id.ratingBar);
        editText = (EditText) findViewById(R.id.editText);

        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendClick(v);
            }
        });

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBoxClick(v);
            }
        });

        btnRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkRadio(v);
            }
        });

        btnBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkRadio(v);
            }
        });

        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
            }
        });

        tglButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tglCheck(v);
            }
        });
    }

    public void sendClick(View v) {
        // Perform action on clicks
        String allText = new String("campo:" + editText.getText());
        allText = allText + ":checkbox:";
        if (checkBox.isChecked()) {
            allText = allText + "Checked:";
        } else {
            allText = allText + "Not Checked:";
        }
        allText = allText + ":toggle:";
        if (tglButton.isChecked()) {
            allText = allText + "Checked:";
        } else {
            allText = allText + "Not Checked:";
        }
        allText = allText + "radios:rojo:";
        String redtext = "";
        if (btnRed.isChecked()) {
            redtext = "pulsado:";
        } else {
            redtext = "no pulsado:";
        }
        allText = allText + redtext;
        allText = allText + "azul";
        String bluetext = "";
        if (btnBlue.isChecked()) {
            bluetext = "pulsado:";
        } else {
            bluetext = "no pulsado:";
        }
        allText = allText + bluetext;
        allText = allText + "rating:";
        float f = ratBar.getRating();
        allText = allText + Float.toString( f ) + ":";
        Log.d("app", allText);
        Toast.makeText(this, allText, Toast.LENGTH_LONG).show();
    }

    public void checkBoxClick(View v) {
        String text = "";
        if (checkBox.isChecked()) {
            text = "Selected";
            salvar.setEnabled(true);
            Toast.makeText(this,"Ya puedes Salvar", Toast.LENGTH_LONG).show();
        } else {
            salvar.setEnabled(false);
            Toast.makeText(this, "Hasta que no marques la casilla no podrás salvar",Toast.LENGTH_LONG).show();
            text = "Not selected";
        }
        Toast.makeText(this,text, Toast.LENGTH_SHORT).show();
    }

    public void checkRadio(View v) {
        boolean checked = ((RadioButton) v).isChecked();

        switch(v.getId()) {
            case R.id.radio1:
                if (checked) {
                    Toast toast = Toast.makeText(this, "RED!", Toast.LENGTH_SHORT);
                    TextView vw = (TextView) toast.getView().findViewById(android.R.id.message);
                    vw.setTextColor(Color.WHITE);
                    toast.getView().setBackgroundColor(Color.RED);
                    toast.show();
                    break;
                }
            case R.id.radio2:
                if (checked) {
                    Toast toast = Toast.makeText(this, "BLUE!", Toast.LENGTH_SHORT);
                    TextView vw = (TextView) toast.getView().findViewById(android.R.id.message);
                    vw.setTextColor(Color.WHITE);
                    toast.getView().setBackgroundColor(Color.BLUE);
                    toast.show();
                    break;
                }
        }
    }

    public void tglCheck(View v){
        Toast.makeText(this, tglButton.getText(), Toast.LENGTH_SHORT).show();
    }
}
